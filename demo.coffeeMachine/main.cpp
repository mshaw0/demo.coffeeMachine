//
//  main.cpp
//  demo.coffeeMachine
//
//

#include <iostream>

// Define abstract base class for coffee types
// This defines the basic interface that all coffee types adhere to
class coffee{
public:
    virtual ~coffee() {};
    virtual void makeCoffee() = 0;
    
    int getPrice(){
        return(price);
    };
    
    void setPrice(const int _price){
        price = _price;
    };
    
    std::string getName(){
        return(name);
    };
    
    void setName(const std::string _name){
        name = _name;
    };
    
    std::string getDescriptor(){
        return(description);
    }
    
    void setDescriptor(const std::string _description){
        description = _description;
    }
    
    int getIngredientMilk(){
        return(ingredientMilk);
    }

    int getIngredientWater(){
        return(ingredientWater);
    }
    
    int getIngredientCoffee(){
        return(ingredientCoffee);
    }
    
protected:
    int price;
    int ingredientMilk;
    int ingredientWater;
    int ingredientCoffee;
    std::string name;
    std::string description;
};

// Defines the Latte Coffee
class Latte : public coffee{
public:
    Latte(){
        std::cout << "Debug: instantiating Latte...\n";
        name = "Latte Morgiano";
        description = "A mix of foamed milk and espresso";
        price = 250;
        ingredientMilk = 200;
        ingredientWater = 100;
        ingredientCoffee = 20;
    }
    
    void makeCoffee(){
        std::cout << "Making a " << name << "\n";
        std::cout << "  - Tsssch...\n";                 // Dispense Foamed Milk
        std::cout << "  - Brrrrrrrr...\n";              // Grid the coffee
        std::cout << "  - Dripdripdripdripdrip...\n";   // Dispense the Espresso
        std::cout << "  - GrrGrrGrrGrrGrr Click\n\n";     // Reset the mechanism and eject grounds
    }
    
};

class Tea : public coffee{
public:
    Tea(){
        std::cout << "Debug: instantiating Tea...\n";
        name = "Tea";
        description = "Basically just hot water with a little milk. Bring your own tea bag.";
        price = 200;
        ingredientMilk = 75;
        ingredientWater = 300;
        ingredientCoffee = 0;
    }
    
    void makeCoffee(){
        std::cout << "Making a " << name << "\n";
        std::cout << "  - Click Dripdripdripdrip...\n"; // Dispense Milk
        std::cout << "  - Tsssch schhhhh...\n";         // Dispense Water
        std::cout << "  - GrrGrrGrrGrrGrr Click\n\n";   // Reset the mechanism and eject grounds
    }
    
};

struct coffeeMachineState{
    int coffee;
    int milk;
    int water;
    int cups;
    int coinFeedBalance;
    int coinBinBalance;
    bool power;
};

struct coffeeMachineConfig{
    int warnCoffee = 100;
    int warnMilk = 500;
    int warnWater = 500;
    int warnCups = 10;
    int warnMoney = 5000;
};

enum coffeeErrors { noError,
                    insufficientMilk,
                    insufficientCoffee,
                    insufficientWater,
                    insufficientCups,
                    insufficientMoney,
                    warnMilk,
                    warnCoffee,
                    warnWater,
                    warnCups,
                    warnMoney };

class coffeeMachine{
private:
    std::vector<coffee *> menu;
    coffeeMachineState state;
    coffeeMachineConfig config;
    
    void updateState(coffee * recipe){
        state.milk -= recipe->getIngredientMilk();
        state.coffee -= recipe->getIngredientCoffee();
        state.water -= recipe->getIngredientWater();
        state.cups--;
        state.coinBinBalance += recipe->getPrice();
        state.coinFeedBalance -= recipe->getPrice();
    }
    
    void printReport(){
        std::cout << "\n == System Report  == \n";
        std::cout << "  Milk volume: " << state.milk << "mL\n";
        std::cout << " Water volume: " << state.water << "mL\n";
        std::cout << "Coffee Weight: " << state.coffee << "g\n";
        std::cout << "         Cups: " << state.cups << "\n";
        std::cout << "         Cash: " << state.coinBinBalance << "\n\n";
    }
    
    void dispenseChange(){
        if(state.coinFeedBalance > 0){
            std::cout << "Dispensing Change: " << state.coinFeedBalance << "\n";
            state.coinFeedBalance = 0;
        }
        else{
            std::cout << "No change to dispense\n";
        }
    }
    
    int checkIngredients(coffee * recipe){
        if(recipe->getIngredientMilk() > state.milk){
            return(insufficientMilk);
        }
        else if(recipe->getIngredientWater() > state.water){
            return(insufficientWater);
        }
        else if(recipe->getIngredientCoffee() > state.coffee){
            return(insufficientCoffee);
        }
        else if(state.cups <= 0){
            return(insufficientCups);
        }
        else if(recipe->getPrice() > state.coinFeedBalance){
            return(insufficientMoney);
        }
        return(noError);
    }
    
    void printHelp(){
        std::cout << "\n  == Help ==  \n" <<
                     "report: list current machine state\n" <<
                     "menu: print coffee options\n" <<
                     "deposit: deposit 500 credits\n\n";
    };
    
    void doDeposit(){
        state.coinFeedBalance += 500;
        std::cout << "Coins deposited\n";
    };
    
    void doCoffeeLogic(std::vector<coffee *>::iterator iter){
        int vendable = checkIngredients((*iter));
        if(vendable == noError){
            updateState(*iter);
            (*iter)->makeCoffee();
            dispenseChange();
            std::cout << "Enjoy your Coffee!\n\n";
        }
        else{
            std::cout << "Sorry, a problem occurred\n";
            dispenseChange();
        }
    }
    
public:
    coffeeMachine(){
        state = {   .milk = 3000,
                    .water = 5000,
                    .coffee = 1000,
                    .cups = 50,
                    .coinFeedBalance = 0,
                    .coinBinBalance = 0
        };
        
        config = {
                    .warnMilk = 500,
                    .warnWater = 500,
                    .warnCoffee = 100,
                    .warnMoney = 5000
        };
        
        state.power = true;
    }
    
    ~coffeeMachine(){
        for(auto iter = menu.begin(); iter != menu.end(); ++iter){
            std::cout << "Debug: deleting coffee: " << (*iter)->getName() << "\n";
            delete(*iter);
        }
    }
    
    void addRecipe(coffee * newCoffee){
        menu.push_back(newCoffee);
    }
    
    void printOptions(){
        std::cout << "Menu:\n";
        for(auto iter = menu.begin(); iter != menu.end(); ++iter){
            std::cout <<
                (*iter)->getName() <<
                ": " <<
                (*iter)->getDescriptor() <<
                " (" << (*iter)->getPrice() <<
                ") \n";
        }
    }
    
    void shutdown(){
        std::cout << "Shutting down...\n";
        state.power = false;
    }
    
    bool processInput(std::string input){
        if(state.power == false){
            std::cout << "Machine is Off\n";
            return(false);
        }
        if(input == "report"){
            printReport();
            return(true);
        }
        if(input == "menu"){
            printOptions();
            return(true);
        }
        if(input == "deposit"){
            doDeposit();
            return(true);
        }
        if(input == "help"){
            printHelp();
            return(true);
        }
        if(input == "shutdown"){
            shutdown();
            return(false);
        }
            
        for(auto iter = menu.begin(); iter != menu.end(); ++iter){
            if(input == (*iter)->getName()){
                doCoffeeLogic(iter);
                return(true);
            }
        }
        
        std::cout << "Sorry, I couldn't understand " << input << "\n";
        return(true);
    }
};

int main(int argc, const char * argv[]) {
    // insert code here...
    std::string input;
    
    coffeeMachine machine;
    
    coffee * B = new Tea();
    machine.addRecipe(B);
    
    coffee * C = new Latte();
    machine.addRecipe(C);
    
    coffee * D = new Latte();
    D->setName("Decaff Latte");
    D->setDescriptor("Like a Latte, but without the Caffeine");
    D->setPrice(300);
    machine.addRecipe(D);
    
    do{
        machine.printOptions();
        std::cout << "Enter Choice: ";
        std::getline(std::cin, input);
        std::cin.clear();
        std::cin.sync();
    }
    while(machine.processInput(input));
    //C->makeCoffee();
    
    return 0;
}
